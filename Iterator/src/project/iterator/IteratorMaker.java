package project.iterator;

public interface IteratorMaker {
    Iterator iterator();
}
