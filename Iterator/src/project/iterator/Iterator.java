package project.iterator;

public interface Iterator {
    boolean hasNext();
    int[] next();
}
