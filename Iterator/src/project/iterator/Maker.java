package project.iterator;

public class Maker implements IteratorMaker{
    private Iterator i;

    public Maker(Iterator i) {
        this.i = i;
    }

    @Override
    public Iterator iterator() {
        return i;
    }
}
