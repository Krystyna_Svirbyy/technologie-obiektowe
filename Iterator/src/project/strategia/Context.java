package project.strategia;
import project.iterator.Iterator;

import java.util.ArrayList;
import java.util.List;

public class Context {
    private Sort currentSort;

    public Sort getCurrentSort() {
        return currentSort;
    }

    public void QuickS(){
        currentSort = new QuickSort();
    }
    public void HeapS(){ currentSort =  new HeapSort();
    }

    public List<int[]> Iterator(Iterator iterator){
        List<int[]> newCollection = new ArrayList<>();
        if (iterator != null){
            while (iterator.hasNext()){
                int tablica[] = iterator.next();
                newCollection.add(currentSort.sort(tablica));
            }
        }
        return newCollection;
    }

}



