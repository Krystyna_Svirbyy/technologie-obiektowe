package project;
import project.iterator.*;
import project.strategia.Context;
import project.strategia.QuickSort;
import java.util.List;
import java.util.ArrayList;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		Random generator = new Random();
		IntIterator iterator = new IntIterator();
		Context context = new Context();

		for (int i = 0; i < 10; i++) {
			int[] tab = new int[10];
			for (int j = 0; j < 10; j++) {
				tab[j] = generator.nextInt(50);
				System.out.print(tab[j] + "\t");
			}
			iterator.addObject(tab);
			System.out.println();
		}


		List<int[]> newList = new ArrayList<>();
		IteratorMaker maker = new Maker(iterator);

		context.HeapS();
		newList = context.Iterator(maker.iterator());


		System.out.println();
		for (int[] obj : newList) {
			for (int i = 0; i < obj.length; i++)
				System.out.print(obj[i] + "\t");
			System.out.println();
		}


	}
}
