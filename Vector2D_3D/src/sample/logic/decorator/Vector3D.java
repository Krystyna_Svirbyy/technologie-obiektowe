package sample.logic.decorator;
import sample.logic.*;

abstract public class Vector3D {
    private Vector2D vector2D;
    private double z;

    public Vector3D(Vector2D vector2D, double z){
        this.vector2D = vector2D;
        this.z = z;
    }

    public Vector2D getVector2D() {
        return vector2D;
    }

    public void setVector2D(Vector2D vector2D) {
        this.vector2D = vector2D;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
    abstract double scalar(Vector3D vector3D);
    abstract double vector(Vector3D vector3D);

}
