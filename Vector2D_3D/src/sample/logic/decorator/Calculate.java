package sample.logic.decorator;

import sample.logic.Algebra;
import sample.logic.Vector2D;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Calculate extends Vector3D implements Algebra {
    public Calculate(Vector2D vector2D, double z) {
        super(vector2D, z);
    }

    @Override
    double scalar(Vector3D vector3D) {
        return vector3D.getVector2D().getX() * super.getVector2D().getX() + super.getVector2D().getY()*vector3D.getVector2D().getY()+ super.getZ() * vector3D.getZ();

    }

    @Override
    double vector(Calculate vector3D) {
        Calculate calculate = new Calculate(new Vector2D(super.getVector2D().getY() * vector3D.getZ() - super.getZ() * vector3D.getVector2D().getY(),
                super.getZ() * vector3D.getVector2D().getX() - super.getVector2D().getX() * vector3D.getZ()), super.getVector2D().getX() * vector3D.getVector2D().getY()
                - super.getVector2D().getY() * vector3D.getVector2D().getX());
        return calculate;
    }



    @Override
    public double abs() {
        return sqrt(pow(this.getVector2D().getX(),2) + pow(this.getVector2D().getY(),2)+ pow(super.getZ(),2));
    }
}
