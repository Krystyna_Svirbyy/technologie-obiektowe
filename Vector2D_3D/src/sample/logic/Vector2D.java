package sample.logic;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Vector2D implements Algebra {
    private double x;
    private double y;

    public Vector2D(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }



    @Override
    public double abs() {
        return sqrt(pow(x,2)+pow(y,2));
    }
}
