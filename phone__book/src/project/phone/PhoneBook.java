package project.phone;

import java.util.ArrayList;
import java.util.List;

public class PhoneBook {
    private NumberFactory phoneRepo = new NumberFactory();
    private NameFactory nameRepo = new NameFactory();

    public void add(String number, String ... names) {
        List<Name> peronNames = new ArrayList<Name>();
        for (String nameString : names) {
            Name personName = nameRepo.findName(nameString);
            peronNames.add(personName);
        }
        phoneRepo.add(number, peronNames);
    }

    public String find(String ... names) {
        List<Name> peronNames = new ArrayList<Name>();
        for (String nameString : names) {
            peronNames.add(new Name(nameString));
        }
        String found = phoneRepo.find(peronNames);
        return found;
    }
}
