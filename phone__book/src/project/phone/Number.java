package project.phone;

public class Number {
    private String phoneNumber;

    public Number(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNumber() {
        return phoneNumber;
    }
}
