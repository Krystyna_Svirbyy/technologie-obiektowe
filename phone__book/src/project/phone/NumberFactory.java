package project.phone;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NumberFactory {
    private Map<String, List<Name>> map = new HashMap<String, List<Name>>();

    public void add(String number, List<Name> names) {
        map.put(number, names);
    }

    public String find(List<Name> names) {
        for (Map.Entry<String, List<Name>> entry : map.entrySet()) {
            if (entry.getValue().containsAll(names)) {
                return entry.getKey();
            }
        }
        return "";
    }

}
