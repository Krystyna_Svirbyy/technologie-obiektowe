package project.phone;
import java.util.HashSet;
import java.util.Set;

public class NameFactory {
    private Set<Name> nameSet = new HashSet<Name>();

    public Name findName(String name) {
        for (Name x : nameSet) {
            if (x.getName().equals(name)) {
                return x;
            }
        }
        Name wrappedName = new Name(name);
        nameSet.add(wrappedName);
        return wrappedName;
    }
}
