package project;
import project.phone.*;

public class Main {

    public static void main(String[] args) {

        PhoneBook book = new PhoneBook();

        book.add("12342", "Jan", "Kowalski");
        book.add("00000", "Jan", "Szym");
        book.add("12345", "Joanna", "Kowalska");
        book.add("12346", "Marcin", "Kowalski");
        String found = book.find( "Jan");
        System.out.println(found);


    }
}
