package com.company;

import com.company.flyweight.Name;
import com.company.flyweight.NameFactory;
import com.company.flyweight.PhoneNumber;
import com.company.flyweight.PhoneNumberFactory;

public class Main {

    public static void main(String[] args) {
	// write your code here
        NameFactory names = new NameFactory();
        Name n1 = new Name("Krystyna");
        names.addName(n1);
        Name n2 = new Name("Imie");
        names.addName(n2);


        PhoneNumberFactory f = new PhoneNumberFactory();

        f.getPhoneNumberHashMap().put(n1, new PhoneNumber("Svirbyy"));
        f.addNumber(n1, "98989898900");
        f.addNumber(n1, "7678987809");
        System.out.println(n1.getName()+ " " + f.getPhoneNumberHashMap().get(n1).getNumber());


        f.getPhoneNumberHashMap().put(n2, new PhoneNumber("Nazwisko"));
        f.addNumber(n2, "Andriowych");
        f.addNumber(n2, "9894090490");
        f.addNumber(n2, "879090093240");

        System.out.println(n2.getName()+ " " + f.getPhoneNumberHashMap().get(n2).getNumber());
        

    }
}
