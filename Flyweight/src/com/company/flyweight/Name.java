package com.company.flyweight;

public class Name extends NameFactory{
    private String name;

    public Name(String name) {
        this.name = name;
    }

    public Name() {

    }


    public String getName() {
        return name;
    }
}
