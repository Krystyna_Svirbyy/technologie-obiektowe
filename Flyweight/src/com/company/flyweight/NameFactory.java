package com.company.flyweight;
import java.util.ArrayList;
import java.util.List;

public class NameFactory {
   private List<Name> names;

    public NameFactory() {
        names = new ArrayList<>();
    }

    public void addName(Name name){
        names.add(name);
    }

    public Name searchName(String name){
        for(Name object: this.names){
            if(name.equals(object.getName())){
                return object;
            }
        }
        return null;
    }
}
