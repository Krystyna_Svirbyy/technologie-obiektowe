package com.company.flyweight;

import java.util.HashMap;
import java.util.Map;

public class PhoneNumberFactory extends Name{
    private Map<Name,PhoneNumber> phoneNumberHashMap;

    public PhoneNumberFactory() {
        super();
        phoneNumberHashMap = new HashMap<>();
    }

    public Map<Name, PhoneNumber> getPhoneNumberHashMap() {
        return phoneNumberHashMap;
    }


    public void addNumber(Name name, String phone){
        phoneNumberHashMap.get(name).AddNum(phone);
    }

}
