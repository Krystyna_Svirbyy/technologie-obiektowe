package com.company.flyweight;

public class PhoneNumber {
    private String number;

    public PhoneNumber(String number) {
        this.number = number;
    }

    public void AddNum(String n){
        number = number + " " + n;
    }


    public String getNumber() {
        return number;
    }
}
