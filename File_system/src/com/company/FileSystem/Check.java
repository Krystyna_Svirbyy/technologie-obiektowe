package com.company.FileSystem;

public class Check {
    private File file;
    public Check(File file){
        this.file = file;
    }

    public boolean checkName(String name){
        return name.matches("[a-zA-Z-0-9-.]+");
    }

    public void setName(String name){
        if(!checkName(name))
            System.out.println("Wrong name");

        file.setName(name);
    }

    public String getName(){
        return file.getName();
    }
}
