package com.company.FileSystem;
import java.util.ArrayList;

public class Folder extends File{
    private File parent;
    private ArrayList<File> files;

    public Folder(){
        this.files = new ArrayList<File>();
    }

    public void addFile(File newFile) {
        files.add(newFile);
    }

    public File readFile(String filename){
        for(File object: this.files){
            if(filename.equals(object.getName())){
                return object;
            }
        }
        return null;
    }


    public File getParent() {
        return parent;
    }

    public void setParent(File parent) {
        this.parent = parent;
    }

    public ArrayList<File> getFiles() {
        return files;
    }

    public void setFiles(ArrayList<File> files) {
        this.files = files;
    }
}
