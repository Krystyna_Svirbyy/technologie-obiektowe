package com.company.FileSystem;

public class CheckFolder extends Folder{
    File file;
    Check check;

   public CheckFolder(String name, File parent) {
        super();
        file = new File();
        check = new Check(file);
        setParent(parent);
        if(findElementByName(name))
            setName(name);
        else
            setName("Name");

    }

    public boolean findElementByName(String name){
        if(((Folder)getParent()).readFile(name) == null){
            return true;
        }
        return false;
    }

    @Override
    public void addFile(File file){
        if(!findElementByName(file.getName())){
            System.out.println("Wrong name");
        }
        super.addFile(file);

    }

   public void ls(){
        System.out.println("/"+getName());
        if(!getFiles().isEmpty()){
            for (File object: getFiles()) {
                if(object instanceof Plik){
                    System.out.println("\t"+object.getName());
                }else {
                    System.out.println("\t"+"/"+object.getName());
                }
            }
        }
    }

    public void tree(int i){
        if(i==0)
            System.out.println("/"+getName());
        if(!getFiles().isEmpty()){
            for (File object: getFiles()) {
                if(object instanceof Plik){
                    for (int j = 0; j <= i; j++)
                        System.out.print("\t");
                    System.out.println(object.getName());
                }else {
                    for (int j = 0; j <= i; j++)
                        System.out.print("\t");
                    System.out.println("/"+object.getName());
                    ((CheckFolder)object).tree(i+1);
                }
            }
        }else{
            for (int j = 0; j <= i; j++)
                System.out.print("\t");
            System.out.println(" ");
        }
    }


}
