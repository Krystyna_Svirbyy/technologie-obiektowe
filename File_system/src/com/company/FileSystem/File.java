package com.company.FileSystem;

public class File {
    private String name;

    File(){}

    File(String name){
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
