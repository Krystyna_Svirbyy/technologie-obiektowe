package com.company;
import com.company.FileSystem.*;

public class Main {

    public static void main(String[] args) {
        Folder filesystem = new Folder();
        filesystem.setName("fileSystem");


        CheckFolder root = new CheckFolder("root", filesystem);
        System.out.println("Create " + root.getName());

        CheckFolder dir1 = new CheckFolder("Dir1", root);
        root.addFile(dir1);
        CheckFolder dir2 = new CheckFolder("Dir2", root);
        root.addFile(dir2);
        CheckFolder dir3 = new CheckFolder("Dir3", root);
        root.addFile(dir3);

        System.out.println("Create in Directory " + root.getName() + ": " + dir1.getName() + "  " + dir2.getName() +"  "+ dir3.getName());

        Plik f1 = new Plik();
        f1.setName("File1");
        f1.setParent(dir1);
        dir1.addFile(f1);


        Plik f2 = new Plik();
        f2.setName("File2");
        f2.setParent(dir2);
        dir2.addFile(f2);

        Plik f3 = new Plik();
        f3.setName("File3");
        f3.setParent(dir3);
        dir3.addFile(f3);


        System.out.println("Create in Directory " + f1.getParent().getName() + " File " + f1.getName());
        System.out.println("Create in Directory " + f2.getParent().getName() + " File " + f2.getName());
        System.out.println("Create in Directory " + f3.getParent().getName() + " File " + f3.getName());

        System.out.println("tree()");
        root.tree(0);
        System.out.println("ls() in root");
        root.ls();
        System.out.println("ls() in dir1");
        dir1.ls();



    }
}
